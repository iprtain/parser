# frozen_string_literal: true

require_relative '../log_parser'
require_relative '../log_store'

RSpec.describe LogParser do
  describe '#perform' do
    let(:store) { LogStore.new }
    let(:parser) { LogParser.new(store: store) }
    
    it 'saves string elements into object attributes' do
      parser.perform(line: '/help_page/1 126.318.035.038')
      
      expect(store.get_webpages[0].url).to eq('/help_page/1')
      expect(store.get_webpages[0].ip).to eq(['126.318.035.038'])
      
    end
  end
end
