# frozen_string_literal: true

require_relative '../log_store'
require_relative '../log_reader'
require_relative '../log_parser'

RSpec.describe LogStore do

  describe '#save_webpage' do
    let(:store) { LogStore.new }

    it 'pushes argument into store attribute webpages(array)' do
      store.save_webpage(webpage: 'one element')
      expect(store.get_webpages).to eq(['one element'])
      expect(store.get_webpages.size).to eq(1)

      store.save_webpage(webpage: 'second element')
      expect(store.get_webpages).to eq(['one element', 'second element'])
      expect(store.get_webpages.size).to eq(2)
    end

    context 'when used inside class instance reader' do
      let(:reader) { LogReader.new(parser: LogParser.new(store: store), filename: 'spec/fixtures/test.log') }
      
      it 'increases store.webpages length to 6' do
        reader.perform

        expect(store.get_webpages.size).to eq(4)
      end
    end
  end

  describe '#get_visits' do
    let(:store) { LogStore.new }
    let(:reader) { LogReader.new(parser: LogParser.new(store: store), filename: 'spec/fixtures/test.log') }

    it 'returns number of visits from top to bottom' do
      reader.perform
      store.get_visits

      expect(store.get_webpages[0].amaunt).to eq(3)
      expect(store.get_webpages[-1].amaunt).to eq(1)
    end
  end

  describe '#check?(arg)' do
    let(:store) { LogStore.new }
    let(:webpage_single) { Struct.new(:url, :ip, :amaunt) }
    
    context 'arg exists in store.webpages list' do
      
      it 'returns false' do
        store.save_webpage(webpage: webpage_single.new('pimpmyride', [123]) )

        expect(store.check?('pimpmyride')).to eq(false)
      end
    end
    context 'arg does not exist in store.webpages list' do
      
      it 'returns true' do
        expect(store.check?('pimpmyride')).to eq(true)
      end
    end
  end
end
