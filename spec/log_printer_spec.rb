# frozen_string_literal: true

require_relative '../log_printer'
require_relative '../log_store'
require_relative '../log_reader'
require_relative '../log_parser'

RSpec.describe LogPrinter do
  before(:example) do
    @store = LogStore.new
    @printer = LogPrinter.new(input_data: @store)
    @reader = LogReader.new(parser: LogParser.new(store: @store), filename: 'spec/fixtures/printer_test.log')
    @reader.perform
  end

  describe '#print_viewes' do
    it 'does smth' do
      
      expect { @printer.print_viewes }.to output(<<~MESSAGE).to_stdout
        /help_page/1 3 visits
        /contact 1 visits
      MESSAGE
    end
  end

  describe '#print_uniq' do
    it 'does smth' do
      expect { @printer.print_uniq }.to output(<<~MESSAGE).to_stdout
        /help_page/1 2 unique viewes
        /contact 1 unique viewes
      MESSAGE
    end
  end
end


