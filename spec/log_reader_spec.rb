require_relative '../log_reader'
require_relative '../log_parser'
require_relative '../log_store'

RSpec.describe LogReader do
    
    describe '#perform' do
        let(:store) { LogStore.new }
        let(:reader) {LogReader.new(parser: LogParser.new(store: store), filename: 'spec/fixtures/test1.log') }

        it 'reads file lines and and uses parser' do
            reader.perform

            expect(store.get_webpages[0].url).to eq('/help_page/1')
            expect(store.get_webpages[0].ip).to eq(['126.318.035.038','126.777.696.038'])
            
        end
    end
end