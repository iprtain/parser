# frozen_string_literal: true

# reads assigned file and extracts information into store class
class LogParser

  def initialize(store: store)
    @store = store
    @webpage_single = Struct.new(:url, :ip, :amaunt)
  end

  def perform(line:)
    url, ip = line.split(' ')

    if store.check?(url) 
      store.save_webpage(webpage: webpage_single.new(url, [ip])) 
    else
      store.find_target(url)[0].ip.push(ip)
    end

  end

  private

  attr_accessor :store, :record_single, :webpage_single

end
