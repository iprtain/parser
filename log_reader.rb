
class LogReader
    def initialize(parser:, filename:)
        @parser = parser
        @file = File.new(filename)
    end

    def perform
        file.each_line do |line|
        	parser.perform(line: line)
        end
    end
    
    private

    attr_reader :parser, :file

end
