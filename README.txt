# ruby_app
ruby_app is a practice project for parsing web log. It takes a file containing urls and ip 
adresses and shows number of viewes and number of unique viewes for every url.

## Installation
Just copy the folder app3_rubo wherever you want.
bundle install

## Usage
ruby parser.rb nameofthefile

