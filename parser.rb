# frozen_string_literal: true

require_relative 'log_parser'
require_relative 'log_store'
require_relative 'log_printer'
require_relative 'log_reader'

raise ArgumentError, 'unknown file' unless File.exist?(ARGV.first)

store = LogStore.new

reader = LogReader.new(parser: LogParser.new(store: store), filename: ARGV.first)

reader.perform

printer = LogPrinter.new(input_data: store)

puts ''

printer.print_viewes

puts ''


printer.print_uniq

