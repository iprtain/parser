# frozen_string_literal: true

# storage for proccesing information, CRUD functionality
class LogStore

  def initialize
    @webpages = []
  end

  def save_webpage(webpage:)
    webpages.push(webpage)
  end

  def get_webpages
    @webpages
  end

  def get_visits
    count
    set_order
  end

  def get_uniq
    webpages.each do |webpage|
      webpage.amaunt = webpage.ip.uniq.size
    end
    set_order
  end

  def check?(url)
    webpages.select { |webpage| webpage.url == url}.empty?
  end

  def find_target(url)
    webpages.select { |webpage| webpage.url == url }
  end

  private

  def count
    webpages.each do |webpage|
      webpage.amaunt = webpage.ip.size
    end
  end

  def set_order
    webpages.sort_by(&:amaunt).reverse
  end

  attr_reader :webpages, :visits, :uniq_viewes
  
end

