# frozen_string_literal: true

# prints provided argument
class LogPrinter
  def initialize(input_data:)
    @input_data = input_data
  end

  def print_viewes
    input_data.get_visits.each { |webpage| puts "#{webpage.url} #{webpage.amaunt} visits" }
  end

  def print_uniq
    input_data.get_uniq.each { |webpage| puts "#{webpage.url} #{webpage.amaunt} unique viewes" }
  end

  private

  attr_accessor :input_data
  
end
